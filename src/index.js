import { createRoot } from "react-dom/client";

import Table from "./components/Table";
import issues from "./data/issues.json";

const rootElement = document.getElementById("root");
const root = createRoot(rootElement);

root.render(<Table issues={issues} />);
