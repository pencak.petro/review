import { useEffect, useRef, useState } from "react";

import TableHeader from "../TableHeader";
import TableRow from "../TableRow";

import classes from "./Table.module.css";

export const isIssueOpened = (status) => status === "open";

function Table({ issues }) {
  const [issuesState, setIssuesState] = useState(
    issues.map((i) => ({ ...i, checked: false }))
  );

  const ref = useRef();

  const [allSelected, setAllSelected] = useState(false);

  const countOfSelectedIssues = issuesState.filter((i) => i.checked).length;
  const countOfOpenedIssues = useRef(
    issuesState.filter((i) => isIssueOpened(i.status)).length
  );

  useEffect(() => {
    if (countOfSelectedIssues === countOfOpenedIssues.current) {
      setAllSelected(true);
    }

    if (!ref.current) {
      return;
    }

    ref.current.indeterminate =
      countOfSelectedIssues > 0 &&
      countOfSelectedIssues < countOfOpenedIssues.current;
  }, [countOfSelectedIssues, countOfOpenedIssues.current]);

  const handleUpdateIssue = (id) => {
    setAllSelected(false);

    const updatedList = issuesState.map((issue) => {
      if (issue.id === id) {
        return {
          ...issue,
          checked: !issue.checked,
        };
      }
      return issue;
    });

    setIssuesState(updatedList);
  };

  const handleSelectAll = (event) => {
    const { checked } = event.target;
    setAllSelected(checked);
    const updatedIssuesState = issuesState.map((issue) => {
      if (isIssueOpened(issue.status)) {
        return { ...issue, checked };
      }
      return issue;
    });
    setIssuesState(updatedIssuesState);
  };

  return (
    <table className={classes.table}>
      <TableHeader
        ref={ref}
        title={
          countOfSelectedIssues
            ? `Selected ${countOfSelectedIssues}`
            : "Nothing selected"
        }
        handleSelectAll={handleSelectAll}
        allSelected={allSelected}
      />
      <tbody>
        {issuesState.map((issue) => (
          <TableRow
            key={issue.id}
            issue={issue}
            handleSelect={() =>
              isIssueOpened(issue.status) ? handleUpdateIssue(issue.id) : null
            }
          />
        ))}
      </tbody>
    </table>
  );
}

export default Table;
