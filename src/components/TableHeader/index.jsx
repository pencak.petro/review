import React, { forwardRef } from "react";

import classes from "./TableHeader.module.css";

const TABLE_HEADER_COLUMNS = ["", "Name", "Message", "Status"];

const TableHeader = ({ title, handleSelectAll, allSelected }, ref) => (
  <thead className={classes.thead}>
    <tr>
      <th className={classes.th}>
        <input
          ref={ref}
          type={"checkbox"}
          className={classes.checkbox}
          checked={allSelected}
          onChange={handleSelectAll}
        />
      </th>
      <th className={classes.numChecked}>{title}</th>
    </tr>
    <tr>
      {TABLE_HEADER_COLUMNS.map((c, index) => (
        <th key={index}>{c}</th>
      ))}
    </tr>
  </thead>
);

export default forwardRef(TableHeader);
