import React from "react";

import { isIssueOpened } from "../Table";

import { classNames } from "../../helpers/classnames.js";

import classes from "./TableRow.module.css";

const TableRow = ({
  issue: { name, message, status, checked },
  handleSelect,
}) => (
  <tr
    className={classNames(
      classes[isIssueOpened(status) ? "openIssue" : "resolvedIssue"],
      { [classes.checked]: checked }
    )}
    onClick={handleSelect}
  >
    <td>
      <input
        className={classes.checkbox}
        type={"checkbox"}
        checked={checked}
        onChange={handleSelect}
        disabled={!isIssueOpened(status)}
      />
    </td>
    <td>{name}</td>
    <td>{message}</td>
    <td>
      <span
        className={classes[isIssueOpened(status) ? "greenCircle" : "redCircle"]}
      />
    </td>
  </tr>
);
export default TableRow;
