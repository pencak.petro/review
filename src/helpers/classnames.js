export function classNames(...args) {
    const classes = [];
  
    args.forEach((arg) => {
      if (typeof arg === "object") {
        Object.keys(arg).forEach((key) => {
          if (arg[key]) {
            classes.push(key);
          }
          return;
        });
      }
      if (typeof arg === "string") {
        classes.push(arg);
        return;
      }
    });
  
    return classes.join(" ");
  }
  